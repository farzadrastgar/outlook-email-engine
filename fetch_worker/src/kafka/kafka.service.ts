import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as kafka from 'kafka-node';
import { ProduceRequest } from 'kafka-node';

@Injectable()
export class KafkaService {
  private producer: kafka.Producer;
  private fetchConsumer: kafka.ConsumerGroup;
  private notificationConsumer: kafka.ConsumerGroup;
  private kafkaHost: string;

  constructor(private readonly configService: ConfigService) {
    this.kafkaHost = this.configService.get<string>('KAFKA_HOST');
    const client = new kafka.KafkaClient({ kafkaHost: this.kafkaHost });
    this.producer = new kafka.Producer(client);
    const fetchConsumerOptions: kafka.ConsumerGroupOptions = {
      kafkaHost: this.kafkaHost,
      groupId: 'fetch_workers',
      fromOffset: 'latest', // Start consuming from the latest committed offset
    };

    const notificationConsumerOptions: kafka.ConsumerGroupOptions = {
      kafkaHost: this.kafkaHost,
      groupId: 'notification_workers',
      fromOffset: 'latest', // Start consuming from the latest committed offset
    };

    // Create consumers for the fetch_topic and notification_topic
    this.fetchConsumer = new kafka.ConsumerGroup(
      fetchConsumerOptions,
      'fetch_topic',
    );
    this.notificationConsumer = new kafka.ConsumerGroup(
      notificationConsumerOptions,
      'notification_topic',
    );

    // Handle errors
    this.fetchConsumer.on('error', (err) => {
      console.error('Kafka fetchConsumer error:', err);
    });

    this.notificationConsumer.on('error', (err) => {
      console.error('Kafka notificationConsumer error:', err);
    });
  }

  async consumeFetch(callback: (message: any) => Promise<void>) {
    this.fetchConsumer.on('message', async (message) => {
      try {
        await callback(this.parseMessage(message));

        // Commit offset after processing
        this.fetchConsumer.commit((err, data) => {
          if (err) {
            console.error('Error committing offset:', err);
          } else {
            console.log('Offset committed:', data);
          }
        });
      } catch (error) {
        console.error('Error processing message:', error);
      }
    });
  }

  async consumeNotification(callback: (message: any) => Promise<void>) {
    this.notificationConsumer.on('message', async (message) => {
      try {
        await callback(this.parseMessage(message));

        // Commit offset after processing
        this.notificationConsumer.commit((err, data) => {
          if (err) {
            console.error('Error committing offset:', err);
          } else {
            console.log('Offset committed:', data);
          }
        });
      } catch (error) {
        console.error('Error processing message:', error);
      }
    });
  }

  async sendBatch(
    messages: { topic: string; messages: string }[],
  ): Promise<void> {
    this.producer.send(messages, (err, data) => {
      if (err) {
        console.error('Error sending batch message:', err);
      } else {
        console.log('Batch message sent:', data);
      }
    });
  }

  private parseMessage(message: kafka.Message): any {
    const value = message.value;
    if (Buffer.isBuffer(value)) {
      return value.toString(); // Convert Buffer to string
    } else if (typeof value === 'string') {
      return JSON.parse(value); // Assuming message is JSON string
    } else {
      throw new Error('Unsupported message format');
    }
  }

  async closeConsumers() {
    await Promise.all([
      new Promise<void>((resolve, reject) => {
        this.fetchConsumer.close(true, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      }),
      new Promise<void>((resolve, reject) => {
        this.notificationConsumer.close(true, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      }),
    ]);
  }
}
