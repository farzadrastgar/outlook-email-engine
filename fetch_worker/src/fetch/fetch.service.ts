import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { KafkaService } from 'src/kafka/kafka.service';
import { SharedService } from 'src/shared/shared.service';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class FetchService {
  constructor(
    private usersService: UsersService,
    private configService: ConfigService,
    private sharedService: SharedService,
    private kafkaService: KafkaService,
  ) {
    this.init();
  }

  private async init() {
    await this.kafkaService.consumeFetch(this.fetchEmails.bind(this));
    await this.kafkaService.consumeNotification(
      this.handleNotification.bind(this),
    );
  }

  async fetchEmails(message: string) {
    const { userId, top, skip, provider } = JSON.parse(message);
    const user = await this.usersService.findById(userId);
    const { accessToken, id } = user;
    const url = `https://graph.microsoft.com/v1.0/me/messages?$top=${top}&$skip=${skip}`;
    const config = {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };

    const response = await this.sharedService.fetchWithRetryAndRefresh(
      userId,
      url,
      config,
    );

    const messages = [];

    for (let i = 0; i < response.value.length; i++) {
      // Prepare a message for Kafka
      messages.push({
        userId: id,
        provider: provider,
        message: response.value[i],
      });
    }
    const kafkaMessages = {
      topic: 'index_topic',
      messages: JSON.stringify(messages),
    };
    await this.kafkaService.sendBatch([kafkaMessages]);
  }

  async handleNotification(message: string) {
    const { messageId, userId, provider } = JSON.parse(message);
    const user = await this.usersService.findById(userId);
    const { accessToken, id } = user;
    const url = `https://graph.microsoft.com/v1.0/me/messages/${messageId}`;
    const config = {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };

    const response = await this.sharedService.fetchWithRetryAndRefresh(
      userId,
      url,
      config,
    );

    // Prepare a message for Kafka
    const kafkaMessage = {
      topic: 'index_topic',
      messages: JSON.stringify([
        {
          userId: id,
          provider: provider,
          message: response,
        },
      ]),
    };

    await this.kafkaService.sendBatch([kafkaMessage]);
  }
}
