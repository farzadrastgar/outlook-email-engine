import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { UsersModule } from 'src/users/users.module';
import { SharedModule } from 'src/shared/shared.module';
import { KafkaModule } from 'src/kafka/kafka.module';
import { FetchService } from './fetch.service';

@Module({
  imports: [ConfigModule, UsersModule, SharedModule, KafkaModule],
  providers: [FetchService],
})
export class FetchModule {}
