import { Module } from '@nestjs/common';
import { UsersModule } from 'src/users/users.module';
import { SharedService } from './shared.service';

@Module({
  imports: [UsersModule],
  providers: [SharedService],
  exports: [SharedService], // Exporting makes it available to other modules
})
export class SharedModule {}
