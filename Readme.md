# Technology stack:

**NestJS:**
 A progressive Node.js framework for building efficient, scalable, and modular server-side applications.

**Postgres:**
 An open-source relational database management system known for its robustness and extensibility.

**Elasticsearch:**
A distributed, RESTful search and analytics engine capable of storing, searching, and analyzing large volumes of data in real-time.

**Socket:**
An endpoint for real-time, bidirectional communication across a computer network.

**PassportJS:** 
A popular authentication middleware for Node.js that simplifies integrating various authentication mechanisms.

**Docker:** 
A platform that uses containerization to package and run applications in isolated environments.

**Kafka:** 
A distributed event streaming platform capable of handling high-throughput, real-time data feeds, and analytics.

**ngrok:** A tool that creates secure tunnels to localhost, commonly used for exposing local servers to the internet for webhook integration and testing.

**Note:**
 Since the Microsoft Graph API accepts only HTTPS endpoints for webhooks, I utilized Ngrok to create a temporary domain for my localhost. Please note that this webhook will expire each time you restart the app. To receive real-time updates, you'll need to log in to the app again to set up a new subscription. This setup is temporary and is unnecessary in a production environment, where these issues won't occur.

---

# How to run :

1. Clone the Repository

   Open your terminal or command prompt and execute the following command:

git clone https://gitlab.com/farzadrastgar/outlook-email-engine.git

2. Rename .env.example to .env

   Navigate into the cloned repository directory:

cd outlook-email-engine

mv .env.example .env

3. Start Docker Containers

   Ensure Docker Desktop is running. Then, in the project directory, run:

docker-compose up

4. Access the Application

   Once the Docker containers are up and running, open a web browser and go to:

http://localhost:3030/user/add

Feel free to reach out to me at farzadrastgar@gmail.com if you encounter any issues while following these instructions. I'm here to help!

---

# How It Works:

The app consists of three main layers: webserver, fetch workers, and index workers. Each of these layers can be scaled independently. An asynchronous communication model is used to decouple the layers, allowing the system to handle bursts of messages efficiently. Kafka is chosen for its high performance,scalability and availability. The attached sequence diagram illustrates the communication between these layers across two main endpoints.

**Containers**

**Webserver:**
The webserver handles tasks such as user login, subscription creation, job creation and receiving webhook notifications.

**Fetch Worker:**
The fetch worker listens to the fetch_topic on Kafka, fetches emails from the Graph API, and writes them to the index_topic on Kafka.

**Index Worker:**
The index worker listens to the index_topic on Kafka and indexes emails in Elasticsearch.

**Endpoints**

**Login:**
Upon completing login, the app reads the total number of emails for the account and creates jobs for fetching on the fetch_topic in Kafka. It also creates a subscription to receive updates. The fetch workers read these fetching jobs, fetch the emails, and write them to the index_topic to be indexed by the index worker.

**Inbox:**
Displays the emails to the client based on the emails indexed in Elasticsearch. It uses websocket for communication between client and server.

**Webhook:**
After a subscription is created, the Graph API sends updates to the /webhook endpoint. This endpoint creates a job on the notification_topic. The fetch worker then retrieves this job, fetches the relevant data, writes it to the index_topic, and the index worker indexes it in Elasticsearch.
