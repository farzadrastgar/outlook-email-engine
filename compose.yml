version: "3.8"

services:
  webserver:
    build: ./webserver
    ports:
      - "3030:3030"
    env_file:
      - .env
    command: npm run start:dev
    networks:
      - mynetwork
    depends_on:
      - postgres
      - kafka1

  fetch_worker:
    build: ./fetch_worker
    ports:
      - "3040:3040"
    env_file:
      - .env
    command: npm run start:dev
    networks:
      - mynetwork
    depends_on:
      - kafka1

  index_worker:
    build: ./index_worker
    ports:
      - "3050:3050"
    env_file:
      - .env
    command: npm run start:dev
    networks:
      - mynetwork
    depends_on:
      - kafka1
      - elasticsearch
  postgres:
    image: postgres:latest
    container_name: my-postgres
    environment:
      POSTGRES_USER: ${POSTGRES_USER}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
      POSTGRES_DB: ${POSTGRES_DATABASE}
    ports:
      - "5432:5432"
    networks:
      - mynetwork
    volumes:
      - postgres_data:/var/lib/postgresql/data

  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:8.11.3
    environment:
      - discovery.type=single-node
      - ELASTIC_PASSWORD=${ELASTICSEARCH_PASSWORD}
      - xpack.security.enabled=true
    ports:
      - "9200:9200"
    deploy:
      resources:
        limits:
          cpus: "2.0"        
          memory: "4096M"    
    networks:
      - mynetwork
    volumes:
      - es_data:/usr/share/elasticsearch/data
      - ./elasticsearch_init:/usr/share/elasticsearch/init

  zoo1:
    image: confluentinc/cp-zookeeper:7.3.2
    hostname: zoo1
    container_name: zoo1
    ports:
      - "2181:2181"
    networks:
      - mynetwork
    environment:
      ZOOKEEPER_CLIENT_PORT: 2181
      ZOOKEEPER_SERVER_ID: 1
      ZOOKEEPER_SERVERS: zoo1:2888:3888

  kafka1:
    image: confluentinc/cp-kafka:7.3.2
    hostname: kafka1
    container_name: kafka1
    ports:
      - "9092:9092"
      - "29092:29092"
      - "9999:9999"
    environment:
      KAFKA_ADVERTISED_LISTENERS: INTERNAL://kafka1:19092,EXTERNAL://${DOCKER_HOST_IP:-127.0.0.1}:9092,DOCKER://host.docker.internal:29092
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: INTERNAL:PLAINTEXT,EXTERNAL:PLAINTEXT,DOCKER:PLAINTEXT
      KAFKA_INTER_BROKER_LISTENER_NAME: INTERNAL
      KAFKA_ZOOKEEPER_CONNECT: "zoo1:2181"
      KAFKA_BROKER_ID: 1
      KAFKA_LOG4J_LOGGERS: "kafka.controller=INFO,kafka.producer.async.DefaultEventHandler=INFO,state.change.logger=INFO"
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
      KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR: 1
      KAFKA_TRANSACTION_STATE_LOG_MIN_ISR: 1
      KAFKA_JMX_PORT: 9999
      KAFKA_JMX_HOSTNAME: ${DOCKER_HOST_IP:-127.0.0.1}
      KAFKA_AUTHORIZER_CLASS_NAME: kafka.security.authorizer.AclAuthorizer
      KAFKA_ALLOW_EVERYONE_IF_NO_ACL_FOUND: "true"
    networks:
      - mynetwork
    depends_on:
      - zoo1

  ngrok:
    image: wernight/ngrok
    ports:
      - "4040:4040" # Web interface
    networks:
      - mynetwork
    environment:
      - NGROK_AUTHTOKEN=2i3BWxMdoH76VkBp9JUP7d6X8oA_2FKFAtXXtg4167f6daAkY
      - NGROK_REGION=us # Optional, can be us, eu, au, ap, sa, jp, in
    command: ngrok http webserver:3030

networks:
  mynetwork:
    driver: bridge

volumes:
  es_data:
  postgres_data:
