#!/bin/bash

# Wait for Elasticsearch to start
until curl -s -XGET "http://elasticsearch:9200/_cat/health" > /dev/null; do
    sleep 1
done

# Define the index name and mapping
INDEX_NAME="mail"
MAPPING='
{
  "mappings": {
    "properties": {
      "userId": { "type": "keyword" },
      "message": {
        "properties": {
          "@odata.etag": { "type": "text" },
          "id": { "type": "text" },
          "createdDateTime": { "type": "date", "format": "strict_date_optional_time||epoch_millis" },
          "lastModifiedDateTime": { "type": "date", "format": "strict_date_optional_time||epoch_millis" },
          "changeKey": { "type": "text" },
          "categories": { "type": "keyword" },
          "receivedDateTime": { "type": "date", "format": "strict_date_optional_time||epoch_millis" },
          "sentDateTime": { "type": "date", "format": "strict_date_optional_time||epoch_millis" },
          "hasAttachments": { "type": "boolean" },
          "internetMessageId": { "type": "text" },
          "subject": { "type": "text" },
          "bodyPreview": { "type": "text" },
          "importance": { "type": "keyword" },
          "parentFolderId": { "type": "text" },
          "conversationId": { "type": "text" },
          "conversationIndex": { "type": "text" },
          "isDeliveryReceiptRequested": { "type": "boolean" },
          "isReadReceiptRequested": { "type": "boolean" },
          "isRead": { "type": "boolean" },
          "isDraft": { "type": "boolean" },
          "webLink": { "type": "text" },
          "inferenceClassification": { "type": "keyword" },
          "body": {
            "properties": {
              "contentType": { "type": "keyword" },
              "content": { "type": "text" }
            }
          },
          "sender": {
            "properties": {
              "emailAddress": {
                "properties": {
                  "name": { "type": "text" },
                  "address": { "type": "text" }
                }
              }
            }
          },
          "from": {
            "properties": {
              "emailAddress": {
                "properties": {
                  "name": { "type": "text" },
                  "address": { "type": "text" }
                }
              }
            }
          },
          "toRecipients": {
            "properties": {
              "emailAddress": {
                "properties": {
                  "name": { "type": "text" },
                  "address": { "type": "text" }
                }
              }
            }
          },
          "ccRecipients": {
            "properties": {
              "emailAddress": {
                "properties": {
                  "name": { "type": "text" },
                  "address": { "type": "text" }
                }
              }
            }
          },
          "bccRecipients": {
            "properties": {
              "emailAddress": {
                "properties": {
                  "name": { "type": "text" },
                  "address": { "type": "text" }
                }
              }
            }
          },
          "replyTo": {
            "properties": {
              "emailAddress": {
                "properties": {
                  "name": { "type": "text" },
                  "address": { "type": "text" }
                }
              }
            }
          },
          "flag": {
            "properties": {
              "flagStatus": { "type": "keyword" }
            }
          }
        }
      }
    }
  }
}
'

# Create index with mapping
curl -XPUT "http://elasticsearch:9200/$INDEX_NAME" -H 'Content-Type: application/json' -d "$MAPPING"

echo "Index '$INDEX_NAME' created."
