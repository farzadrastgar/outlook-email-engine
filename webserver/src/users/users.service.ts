import { Injectable, NotFoundException } from '@nestjs/common';
import { User } from './entities/users.entity';
import * as _ from 'lodash';
import { UpdateUserDto } from './dto/update-user-dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async create(user: User): Promise<User> {
    const createdUser = this.userRepository.create(user);
    return this.userRepository.save(createdUser);
  }

  async findByEmail(email): Promise<User> {
    return await this.userRepository.findOne({ where: { email } });
  }

  async findById(id: string) {
    return this.userRepository.findOne({ where: { id } });
  }

  async update(id: string, updateUserDto: UpdateUserDto): Promise<User> {
    const user = await this.userRepository.findOne({ where: { id } });
    if (!user) {
      throw new NotFoundException(`User not found`);
    }
    Object.assign(user, updateUserDto);

    return this.userRepository.save(user);
  }
}
