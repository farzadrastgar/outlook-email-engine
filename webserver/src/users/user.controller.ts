import {
  Controller,
  Get,
  Post,
  Body,
  Res,
  Req,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { Response, Request } from 'express';
import { AuthGuard } from '@nestjs/passport';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UsersService) {}

  @Get('add')
  async showAddUserForm(@Res() res: Response) {
    res.render('add-account', { title: 'Create Account' });
  }

  @Post('create')
  async createUser(
    @Body('email') email: string,
    @Res() res: Response,
    @Req() req: Request,
  ) {
    // Redirect to Outlook login

    res.redirect('/auth/outlook?email=' + email);
  }
}
