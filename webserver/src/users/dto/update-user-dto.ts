export class UpdateUserDto {
  accessToken?: string;
  refreshToken?: string;
  fetchedAt?: string;
}
