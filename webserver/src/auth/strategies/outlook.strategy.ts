import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, VerifyCallback } from 'passport-oauth2';
import { AuthService } from '../auth.service';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';
import { Request } from 'express';

@Injectable()
export class OutlookStrategy extends PassportStrategy(Strategy, 'outlook') {
  constructor(
    private authService: AuthService,
    private configService: ConfigService,
  ) {
    super({
      authorizationURL:
        'https://login.microsoftonline.com/common/oauth2/v2.0/authorize',
      tokenURL: 'https://login.microsoftonline.com/common/oauth2/v2.0/token',
      clientID: configService.get('OUTLOOK_APPLICATION_CLIENT_ID'),
      clientSecret: configService.get('OUTLOOK_CLIENT_VALUE'),
      callbackURL: 'http://localhost:3030/auth/outlook/callback',
      scope: ['openid', 'profile', 'offline_access', 'Mail.Read', 'user.read'],
    });
  }

  async userProfile(accessToken, done) {
    try {
      const response = await axios.get('https://graph.microsoft.com/v1.0/me', {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });
      const profile = response.data;
      done(null, profile);
    } catch (err) {
      done(err);
    }
  }

  authenticate(req: Request, options?: any): void {
    super.authenticate(req, { ...options, ...{ email: req.query.email } });
  }

  authorizationParams(options: any): any {
    const params: any = {};
    if (options.email) {
      params.login_hint = options.email;
    }
    return params;
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: any,
    done: VerifyCallback,
  ) {
    try {
      const user = await this.authService.validateOAuthLogin(
        'outlook',
        profile,
        accessToken,
        refreshToken,
      );
      done(null, user);
    } catch (err) {
      done(err, false);
    }
  }
}
