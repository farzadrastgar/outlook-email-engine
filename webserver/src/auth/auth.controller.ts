import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';

import { Request } from 'express';

import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Get('outlook')
  @UseGuards(AuthGuard('outlook'))
  async outlookLogin() {}

  @Get('outlook/callback')
  @UseGuards(AuthGuard('outlook'))
  async outlookLoginCallback(@Req() req, @Res() res) {
    // Handles callback after Outlook OAuth
    const { token } = req.user;
    res.cookie('jwt', token, {
      httpOnly: true,
      secure: false,
    });
    // Redirect back to frontend callback URL
    res.redirect('/outlook/init');
  }
}
