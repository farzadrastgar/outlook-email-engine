import {
  Injectable,
  UnauthorizedException,
  BadRequestException,
  ForbiddenException,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { ConfigService } from '@nestjs/config';
import { User } from 'src/users/entities/users.entity';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  async validateOAuthLogin(
    provider: string,
    profile: any,
    accessToken: string,
    refreshToken: string,
  ): Promise<any> {
    // Extract necessary information from the profile
    const { id, displayName, mail } = profile;

    // Check if the user already exists in your database
    let user: User = await this.usersService.findByEmail(mail);

    if (!user) {
      // If the user does not exist, create a new one
      user = new User();
      user.provider = provider;
      user.providerId = id; // Assuming you have a field for the Outlook ID
      user.name = displayName;
      user.email = mail;
      user.accessToken = accessToken; // Store the access token if necessary
      user.refreshToken = refreshToken; // Store the refresh token if necessary
      user = await this.usersService.create(user);
    } else {
      user = await this.usersService.update(user.id, {
        accessToken,
        refreshToken,
      });
    }

    // Generate a JWT token for the user
    const payload = { sub: user.id, email: user.email };
    const jwtToken = this.jwtService.sign(payload);

    // Return the user object along with the JWT token
    return { user, token: jwtToken };
  }

  async logout(userId: string) {
    return this.usersService.update(userId, { refreshToken: null });
  }
}
