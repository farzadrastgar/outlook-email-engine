import { Injectable } from '@nestjs/common';
import { AxiosInstance, AxiosRequestConfig } from 'axios';
import {
  fetchWithRetry,
  createAxiosInstanceWithRetry,
} from './utils/axios-retry.util';
import { refreshAccessToken } from './utils/token-refresh.util';
import { UsersService } from '../users/users.service'; // Adjust the path as necessary
import { User } from '../users/entities/users.entity'; // Adjust the path as necessary
import { ConfigService } from '@nestjs/config';

@Injectable()
export class SharedService {
  private axiosInstance: AxiosInstance;
  private clientId: string;
  private clientSecret: string;

  constructor(
    private readonly userService: UsersService,
    private configService: ConfigService,
  ) {
    this.axiosInstance = createAxiosInstanceWithRetry();
    // Initialize clientId and clientSecret from config or environment variables
    this.clientId = configService.get('OUTLOOK_APPLICATION_CLIENT_ID');
    this.clientSecret = configService.get('OUTLOOK_CLIENT_VALUE');
  }

  async fetchUserInfo(userId: string): Promise<User> {
    return this.userService.findById(userId);
  }

  async fetchWithRetryAndRefresh(
    userId: string,
    url: string,
    config?: AxiosRequestConfig,
  ): Promise<any> {
    const user = await this.fetchUserInfo(userId);
    let { accessToken, refreshToken } = user;

    if (!config.headers) {
      config.headers = {};
    }
    config.headers['Authorization'] = `Bearer ${accessToken}`;

    try {
      const response = await fetchWithRetry(this.axiosInstance, url, config);
      return response.data;
    } catch (error) {
      if (error.response && error.response.status === 401) {
        // Access token expired, refresh it
        console.log('Access token expired. Refreshing...');
        accessToken = await this.refreshAccessToken(refreshToken, userId);
        config.headers['Authorization'] = `Bearer ${accessToken}`;

        // Retry the request with the new access token
        const response = await fetchWithRetry(this.axiosInstance, url, config);
        return response.data;
      } else {
        throw error;
      }
    }
  }

  private async refreshAccessToken(
    refreshToken: string,
    userId: string,
  ): Promise<string> {
    try {
      const newAccessToken = await refreshAccessToken(
        refreshToken,
        this.clientId,
        this.clientSecret,
      );
      // Update the user's access token in the database
      await this.userService.update(userId, { accessToken: newAccessToken });
      return newAccessToken;
    } catch (error) {
      console.error('Failed to refresh access token:', error);
      throw error;
    }
  }
}
