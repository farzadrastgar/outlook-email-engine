export interface GraphApiError {
  error: {
    code: string;
    message: string;
  };
}

export interface GraphApiResponse<T = any> {
  data: T;
}

export interface GraphApiErrorResponse
  extends GraphApiResponse,
    GraphApiError {}
