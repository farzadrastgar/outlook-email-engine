import axios, {
  AxiosRequestConfig,
  AxiosResponse,
  AxiosInstance,
  AxiosError,
} from 'axios';
import axiosRetry from 'axios-retry';
import { GraphApiErrorResponse } from './interfaces/graph-api-response.interface';

export const createAxiosInstanceWithRetry = (): AxiosInstance => {
  const axiosInstance = axios.create();

  axiosRetry(axiosInstance, {
    retries: 10,
    retryDelay: (retryCount) => {
      return Math.pow(2, retryCount) * 1000; // Exponential backoff: 1s, 2s, 4s
    },
    retryCondition: (error: AxiosError<GraphApiErrorResponse>) => {
      // Retry on network errors or 5xx status codes
      return (
        axiosRetry.isNetworkOrIdempotentRequestError(error) ||
        (error.response &&
          (error.response.status >= 500 ||
            error.response.status === 429 || // Retry on rate limit exceeded (429) errors
            (error.response.data &&
              error.response.data.error &&
              error.response.data.error.code &&
              error.response.data.error.code === 'UnknownError' &&
              error.response.data.error.message &&
              error.response.data.error.message.includes('Rate limit')))) // Retry on specific rate limit messages
      );
    },
  });

  return axiosInstance;
};

export const fetchWithRetry = async (
  axiosInstance: AxiosInstance,
  url: string,
  config?: AxiosRequestConfig,
): Promise<AxiosResponse<any>> => {
  try {
    if (config.method == 'post') {
      const response = await axiosInstance.post(url, config.data, config);
      return response;
    } else {
      const response = await axiosInstance.get(url, config);
      return response;
    }
  } catch (error) {
    console.error('Error fetching data:', error);
    throw error;
  }
};
