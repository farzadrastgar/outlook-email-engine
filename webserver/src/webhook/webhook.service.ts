import { Injectable } from '@nestjs/common';
import axios from 'axios';
import { KafkaService } from 'src/kafka/kafka.service';

@Injectable()
export class WebhookService {
  constructor(private kafkaService: KafkaService) {}
  async getNgrokUrl(): Promise<string> {
    try {
      const response = await axios.get('http://ngrok:4040/api/tunnels');
      const tunnels = response.data.tunnels as Array<{ public_url: string }>;
      if (tunnels.length > 0) {
        return tunnels[0].public_url;
      }
      throw new Error('Ngrok tunnel not found');
    } catch (error) {
      console.error('Error fetching ngrok tunnel:', error);
      throw error;
    }
  }

  // Example method to process webhook events
  async createJob(payload: any) {
    console.log(JSON.stringify(payload));
    const {
      clientState: userId,
      resourceData: { id: messageId },
    } = payload.value[0];

    console.log(messageId, userId);
    const messages = [
      {
        topic: 'notification_topic',
        messages: JSON.stringify({
          messageId,
          userId,
          provider: 'outlook',
          date: new Date(),
        }),
      },
    ];

    return await this.kafkaService.sendBatch(messages);
  }
}
