import { Controller, Post, Body, Req, Res, Query } from '@nestjs/common';
import { WebhookService } from './webhook.service';
import { Response } from 'express';

@Controller('webhook')
export class WebhookController {
  constructor(private readonly webhookService: WebhookService) {}

  @Post()
  async handleValidation(
    @Query('validationToken') validationToken: string,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    if (validationToken) {
      // Respond with the validation token in plain text
      res.status(200).send(validationToken);
    } else {
      await this.webhookService.createJob(req.body);
      res.status(200).send();
    }
  }
}
