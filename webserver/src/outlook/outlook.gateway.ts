import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { OutlookService } from './outlook.service';

@WebSocketGateway()
export class OutlookGateway
  implements OnGatewayConnection, OnGatewayDisconnect
{
  @WebSocketServer()
  server: Server;

  constructor(private readonly outlookService: OutlookService) {}

  async handleConnection(client: Socket) {
    console.log('Client connected', client.id);
  }

  async handleDisconnect(client: Socket) {
    console.log('Client disconnected', client.id);
  }

  @SubscribeMessage('getEmails')
  async handleGetEmails(
    client: Socket,
    payload: { userId: string; page: number; pageSize: number },
  ) {
    const { userId, page, pageSize } = payload;

    const emails = await this.outlookService.getEmails(userId, page, pageSize);
    client.emit('emails', emails);
  }
}
