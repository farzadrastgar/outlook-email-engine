import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { UsersModule } from 'src/users/users.module';
import { SharedModule } from 'src/shared/shared.module';
import { KafkaModule } from 'src/kafka/kafka.module';
import { OutlookService } from './outlook.service';
import { OutlookController } from './outlook.controller';
import { WebhookModule } from 'src/webhook/webhook.module';
import { OutlookGateway } from './outlook.gateway';
import { ElasticsearchModule } from 'src/elasticSearch/elasticSearch.module';

@Module({
  imports: [
    ConfigModule,
    UsersModule,
    SharedModule,
    KafkaModule,
    WebhookModule,
    ElasticsearchModule,
  ],
  providers: [OutlookService, OutlookGateway],
  controllers: [OutlookController],

  exports: [OutlookService],
})
export class OutlookModule {}
