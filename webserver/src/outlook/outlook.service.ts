import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AxiosRequestConfig } from 'axios';
import { ElasticsearchService } from 'src/elasticSearch/elasticSearch.service';
import { KafkaService } from 'src/kafka/kafka.service';
import { SharedService } from 'src/shared/shared.service';
import { UsersService } from 'src/users/users.service';
import { WebhookService } from 'src/webhook/webhook.service';

@Injectable()
export class OutlookService {
  constructor(
    private usersService: UsersService,
    private configService: ConfigService,
    private sharedService: SharedService,
    private kafkaService: KafkaService,
    private webhookService: WebhookService,
    private elasticSearchService: ElasticsearchService,
  ) {}

  async init(userId: string) {
    await this.createJobs(userId);
    await this.createSubscription(userId);
  }

  async createJobs(userId: string) {
    const batchSize: number = +this.configService.get<number>(
      'EMAIL_FETCH_BATCH_SIZE',
    );

    const user = await this.usersService.findById(userId);
    const { id, provider } = user;

    const url = `https://graph.microsoft.com/v1.0/me/messages/$count`;

    const config = {
      headers: {
        'Content-Type': `application/json`,
      },
    };

    const response = await this.sharedService.fetchWithRetryAndRefresh(
      userId,
      url,
      config,
    );

    console.log(response);
    const emailCount = response;
    const messages = [];

    console.log(emailCount);

    for (let skip = 0; skip < emailCount; skip += batchSize) {
      // Prepare a message for Kafka
      messages.push({
        topic: 'fetch_topic',
        messages: JSON.stringify({
          userId: id,
          top: batchSize,
          skip,
          provider,
        }),
      });
    }

    console.log(messages);
    await this.kafkaService.sendBatch(messages);

    await this.usersService.update(userId, {
      fetchedAt: new Date().toISOString(),
    });
  }

  async createSubscription(userId: string) {
    const user = await this.usersService.findById(userId);
    const { accessToken, id } = user;

    const url = 'https://graph.microsoft.com/v1.0/subscriptions';
    const webhookUrl = (await this.webhookService.getNgrokUrl()) + '/webhook';

    const subscriptionPayload = {
      changeType: 'created,updated,deleted',
      notificationUrl: webhookUrl,
      resource: '/me/messages',
      expirationDateTime: new Date(
        new Date().getTime() + 86400000,
      ).toISOString(),
      clientState: id,
    };

    const config: AxiosRequestConfig = {
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await this.sharedService.fetchWithRetryAndRefresh(
        userId,
        url,
        {
          ...config,
          method: 'post',
          data: subscriptionPayload,
        },
      );

      console.log('Subscription created successfully:', response);
    } catch (error) {
      console.error(
        'Error creating subscription:',
        error.response?.data || error.message,
      );
      throw new Error('Failed to create subscription');
    }
  }

  async getEmails(userId: string, page: number, pageSize: number) {
    // Fetch emails from Elasticsearch

    const response = await this.elasticSearchService.search('outlook', {
      from: (page - 1) * pageSize,
      size: pageSize,
      sort: [{ 'message.createdDateTime': { order: 'desc' } }],
      query: {
        match: { userId: userId },
      },
    });
    return response.hits.hits.map((hit) => hit._source);
  }
}
