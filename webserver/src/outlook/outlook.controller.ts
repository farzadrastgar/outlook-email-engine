import { Controller, Get, Render, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { User } from 'src/auth/decorator/user.decorator';
import { OutlookService } from './outlook.service';
import { Response, Request } from 'express';

@Controller('outlook')
export class OutlookController {
  constructor(private readonly outlookService: OutlookService) {}

  @Get('init')
  @UseGuards(AuthGuard('jwt'))
  async outlookLogin(@User() user, @Res() res: Response) {
    await this.outlookService.init(user.userId);
    return res.redirect('/outlook/inbox');
  }

  @Get('inbox')
  @UseGuards(AuthGuard('jwt'))
  async getEmailsPage(@Res() res: Response, @User() user) {
    res.render('email-list', { userId: user.userId });
  }
}
