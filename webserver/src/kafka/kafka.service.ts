import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as kafka from 'kafka-node';
import { ProduceRequest } from 'kafka-node';

@Injectable()
export class KafkaService {
  private producer: kafka.Producer;
  private kafkaHost: string;

  constructor(private readonly configService: ConfigService) {
    this.kafkaHost = this.configService.get<string>('KAFKA_HOST');
    const client = new kafka.KafkaClient({ kafkaHost: this.kafkaHost });
    this.producer = new kafka.Producer(client);
  }

  async sendBatch(
    messages: { topic: string; messages: string }[],
  ): Promise<void> {
    const payloads: ProduceRequest[] = messages.map(({ topic, messages }) => ({
      topic,
      messages: JSON.stringify(messages),
    }));
    this.producer.send(payloads, (err, data) => {
      if (err) {
        console.error('Error sending batch message:', err);
      } else {
        console.log('Batch message sent:', data);
      }
    });
  }
}
