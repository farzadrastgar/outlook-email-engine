import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { KafkaModule } from 'src/kafka/kafka.module';
import { IndexService } from './index.service';
import { ElasticsearchModule } from 'src/elasticSearch/elasticSearch.module';

@Module({
  imports: [ConfigModule, KafkaModule, ElasticsearchModule],
  providers: [IndexService],
})
export class IndexModule {}
