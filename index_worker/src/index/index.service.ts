import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ElasticsearchService } from 'src/elasticSearch/elasticSearch.service';
import { KafkaService } from 'src/kafka/kafka.service';

@Injectable()
export class IndexService {
  constructor(
    private configService: ConfigService,
    private kafkaService: KafkaService,
    private elasticSearchService: ElasticsearchService,
  ) {
    this.kafkaService.consume(this.indexEmails.bind(this));
  }

  async indexEmails(
    messages: { provider: string; userId: string; message: { id: string } }[],
  ) {
    if (messages.length > 1) {
      const body = messages.flatMap((msg) => {
        return [{ index: { _index: msg.provider, _id: msg.message.id } }, msg];
      });

      console.log(body);
      await this.elasticSearchService.bulk(body);
    } else {
      const message = messages[0];

      await this.elasticSearchService.indexDocument(
        message.provider,
        message.message.id,
        message,
      );
    }
  }
}
