import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as kafka from 'kafka-node';

@Injectable()
export class KafkaService {
  private producer: kafka.Producer;
  private consumer: kafka.ConsumerGroup;
  private kafkaHost: string;

  constructor(private readonly configService: ConfigService) {
    this.kafkaHost = this.configService.get<string>('KAFKA_HOST');
    const client = new kafka.KafkaClient({ kafkaHost: this.kafkaHost });
    this.producer = new kafka.Producer(client);
    const consumerOptions: kafka.ConsumerGroupOptions = {
      kafkaHost: this.kafkaHost,
      groupId: 'index_workers',
      fromOffset: 'latest',
    };

    this.consumer = new kafka.ConsumerGroup(consumerOptions, 'index_topic');

    this.consumer.on('error', (err) => {
      console.error('Kafka consumer error:', err);
    });
  }

  async consume(callback: (message: any) => Promise<void>) {
    this.consumer.on('message', async (message) => {
      try {
        const parsedMessage = this.parseMessage(message);
        await callback(parsedMessage);

        this.consumer.commit((err, data) => {
          if (err) {
            console.error('Error committing offset:', err);
          } else {
            console.log('Offset committed:', data);
          }
        });
      } catch (error) {
        console.error('Error processing message:', error);
      }
    });
  }

  private parseMessage(message: kafka.Message): any {
    const value = message.value;
    console.log(value);
    if (Buffer.isBuffer(value)) {
      return JSON.parse(value.toString());
    } else if (typeof value === 'string') {
      return JSON.parse(value);
    } else {
      throw new Error('Unsupported message format');
    }
  }

  async closeConsumer() {
    await new Promise<void>((resolve, reject) => {
      this.consumer.close(true, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
}
