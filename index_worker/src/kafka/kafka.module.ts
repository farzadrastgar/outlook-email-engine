import { Module } from '@nestjs/common';
import { KafkaService } from './kafka.service';

@Module({
  providers: [KafkaService],
  exports: [KafkaService],
})
export class KafkaModule {
  constructor(private readonly kafkaService: KafkaService) {}

  // Clean up resources when the module is destroyed
  async onModuleDestroy() {
    await this.kafkaService.closeConsumer();
  }
}
