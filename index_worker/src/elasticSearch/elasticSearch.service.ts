import { Injectable } from '@nestjs/common';
import { Client } from '@elastic/elasticsearch';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class ElasticsearchService {
  private readonly client: Client;

  constructor(private configService: ConfigService) {
    this.client = new Client({
      node: configService.get('ELASTICSEARCH_NODE'),
      auth: {
        username: configService.get('ELASTICSEARCH_USERNAME'),
        password: configService.get('ELASTICSEARCH_PASSWORD'),
      },
    });
  }

  async indexDocument(index: string, id: string, body: any): Promise<any> {
    return await this.client.index({
      index,
      id,
      body,
    });
  }

  async bulk(body: any[]): Promise<any> {
    return await this.client.bulk({ body });
  }

  async search(index: string, body: any): Promise<any> {
    return await this.client.search({
      index,
      body,
    });
  }
}
