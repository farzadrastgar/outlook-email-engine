import { Module } from '@nestjs/common';
import { ElasticsearchService } from './elasticSearch.service';

@Module({
  providers: [ElasticsearchService],
  exports: [ElasticsearchService], // Export ElasticsearchService to be used in other modules
})
export class ElasticsearchModule {}
